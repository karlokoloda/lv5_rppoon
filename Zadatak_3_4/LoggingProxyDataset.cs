﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3_4
{
    class LoggingProxyDataset : IDataset
    {
        private Dataset dataset;
        private string filePath;

        public LoggingProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            ConsoleLogger.GetInstance().Logger();
            if (dataset == null) {
                dataset = new Dataset(filePath);
            }
            return dataset.GetData();
        }
    }
}
