﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            User Admin = User.GenerateUser("admin");

            IDataset dataset = new ProtectionProxyDataset(Admin);
            DataConsolePrinter printer = new DataConsolePrinter();

            printer.Print(dataset);

            User Marko = User.GenerateUser("marko");
            IDataset datasetNotAllowed = new ProtectionProxyDataset(Marko);
            printer.Print(datasetNotAllowed);


            //cetvrti

            IDataset loggingProxyDataset = new LoggingProxyDataset("logs.txt");
            IList<List<string>> data = loggingProxyDataset.GetData();
        }
    }
}
