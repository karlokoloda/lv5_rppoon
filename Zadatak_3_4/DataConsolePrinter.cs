﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3_4
{
    class DataConsolePrinter
    {
        public void Print(IDataset dataset)
        {
            IList<List<string>> data = dataset.GetData();
            if(data == null) {
                Console.WriteLine("User error! Access denied");
            }
            else {
                Console.WriteLine("Requsted data: ");
                foreach (List<string> row in data)
                {
                    Console.WriteLine(string.Join(" ", row));
                }
            }
        }
    }
}
