﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            ShippingService shippingService = new ShippingService(5);

            Box ShoppingCart = new Box("Personal stuff");

            Product Shirt = new Product("black t-shirt", 30, 0.2);
            ShoppingCart.Add(Shirt);

            Product Jeans = new Product("basic jeans", 200, 0.4);
            ShoppingCart.Add(Jeans);

            Product Shoes = new Product("oxfords not brogues", 500, 0.8);
            ShoppingCart.Add(Shoes);


            
            Console.WriteLine("My shopping cart: " + ShoppingCart.Description() + "\n" + "Total shipping price: " + shippingService.CalculatePrice(ShoppingCart));
        }
    }
}
