﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1_2
{
    class ShippingService
    {
        private double priceKg;

        public ShippingService(double PriceKg) {
            this.priceKg = PriceKg;
        }

        public double CalculatePrice(IShipable shipable) {
            return this.priceKg * shipable.Weight;
        }
    }
}
